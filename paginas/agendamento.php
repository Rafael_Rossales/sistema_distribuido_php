<?php
require '../conexao/conexao.php';
include '../template/header.php';
include '../template/menu.php';
include '../template/footer.php';


$id = $_GET['id'];
?>


<!DOCTYPE html>
<html>
<head>
	<title>Agendamento</title>
</head>
<body> 

<div class="container" style="margin-top:30px; margin-left:10%;">
	<div class="row">
      <div class="col-sm-12">
      	 <form  method="post" action ="dadosAgendamento.php">
      	 	<div class="row">
      	 	<?php
      	 	$query = "SELECT * FROM medico WHERE idmedico ='" .$id."';";
      	 	$result = mysqli_query($dbc, $query) or die(mysqli_error($dbc));
      	 	while($row =mysqli_fetch_array($result)){
      	 		?>
      	 		<div class="col">
      	 			<label>Médico</label>
      	 			<input type="text" class="form-control"  value="<?=$row['nome'];?>" disabled>
      	 		</div>
      	 		<div class="col">
      	 			<label>Especialidade</label>
      	 			<input type="text" class="form-control" value="<?=$row['especializacao'];?>" disabled>
      	 		</div>
      	 	<input type="hidden" name="idMedico" value="<?=$row['idmedico'];?>">
      	  <?php }?>
      	 	</div>

      	 	<div class="row" style="margin-top:20px;">
      	 		<div class="col">
      	 			<label>Paciente</label>
      	 			<input type="text" class="form-control" name="idUsuario" required>
      	 		</div>
      	 		<div class="col">
      	 			<label>Telefone</label>
      	 			<input type="text" class="form-control"  name="telefone" required>
      	 		</div>
      	 	</div>

      	 	<div class="row" style="margin-top:20px;">
      	 		<div class="col">
      	 			<label>Data</label>
      	 			<input type="text" class="form-control" name="data" required>
      	 		</div>
      	 		<div class="col">
      	 			<label>Hora</label>
      	 			<input type="text" class="form-control"  name="hora" required>
      	 		</div>
      	 	</div>
      	 	<div class="row" style="margin-top:20px;">
      	 		<div class="col">
                   <button type="submit" name="salvar" class="btn btn-secondary">Agendar Consulta</button>
                    <button type="reset" name="salvar" class="btn btn-danger">Limpar</button>
      	 		</div>
      	 	</div>
      	 </form>       	 
      </div>
	</div>
</div>
</body>
</html>