<?php
require ('paginas/funcoes.php');
include 'template/header.php';
include 'template/menu.php';
include 'template/footer.php';

$funcoes_data = new funcoes();

if (isset($_POST['submit'])){

	switch ($_POST['request']) {
		case 'Medicos':
			$medicos_info = $funcoes_data->getMedicoNome();
			break;
		case 'Pacientes':
			$medicos_info = $funcoes_data->getPacientes();
			break;
		default:
			http_response_code(400);
			break;
	}
  }elseif(isset($_POST['pesquisar'])) {
         $nome =  $_POST['nome'];      
         $medicos_info =  $funcoes_data->getpesquisarMedico($nome);
     }


?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>
<body>
<div class="container-fluid" style="margin-top:30px; margin-left:15%;">
	<div class="row">
      <div class="col-sm">
      	 <form action ="index.php" method="post">
      	 	 <div class="form-group">
      	 	 	<select class="form-control" name="request">
      	 	 		<option>Medicos</option>
      	 	 		<option>Pacientes</option>
      	 	 		<option>Agendamentos</option>
      	 	 		<option>Serviços</option>
      	 	 	</select>
      	 	 </div> 
      	 	</div>
      	 	<div class="col-sm"> 
      	 		<input type="submit" value="Listar Dados" class="btn btn-warning active" name="submit">
      	 	</form>
             <button type="button" class="btn btn-success active" id="pesquisar">Pesquisar</button>
      </div>
	</div>
</div>
<div class="container-fluid" style="margin-top:5px; margin-left:15%;">
	<div class="row" id="form-pesquisar"style="display: none;">
      <div class="col-sm" >
      	 <form action ="index.php" method="post">
      	 	<div class="form-group">
      	 		<input type="text" class="form-control" name="nome" placeholder="Pesquisar por especialidade" required>
      	 	</div>
      	 	</div>
      	 	<div class="col-sm"> 
      	 		<input type="submit" name="pesquisar" value="Pesquisar">
      	 	</form>
      </div>
	</div>
</div>

<?php
if(!isset($medicos_info)){

}else{
  $medico =  json_encode($medicos_info);
  $dados = json_decode($medico);
}

?>
<div class="row">
  <div class="col"></div>
  <div class="col-9" style="margin-top:20px;">
  	<table class="table table-hover">
	<tr>
		<th>Cod</th>
		<th>Nome</th>
		<th>Email</th>
		<th>Especializacao</th>
		<th>Ações</th>
	</tr>
<?php 
foreach ($dados as $info) {?>
	<tr>
	<td><?php echo $info->idmedico;?></td>
	<td><?php echo $info->nome;?></td>
	<td><?php echo $info->email;?></td>
	<td><?php echo $info->especializacao;?></td>
	<td>
		 <a href="paginas/agendamento.php?id=<?php echo $info->idmedico;?>" class="btn btn-primary">Agendar consulta</a>
	</td>
    </tr>
	
<?php }?>
</table>
  </div>
  <div class="col"></div>
</div>

</body>
<script>
$(document).ready(function(){
  $("#pesquisar").click(function(){
    $("#form-pesquisar").toggle();
  });
});
</script>
</html>
