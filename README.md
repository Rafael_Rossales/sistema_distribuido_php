<h1>Sistema Distribuído</h1>

<h2>Introdução</h2>
  <p>O sistema de agendamento e administração de consultas permitirá aos usuários realizar o agendamento de consultas.
Os agendamentos serão feitos de acordo com as especialidades requeridas pelo usuário, além do agendamento os usuários podem
realizar busca através das especialidades desejadas. Após concluído o agendamento será enviado um sms de confirmação para o 
usuário para que haja a verificação do seu número, e ao mesmo tempo mantê-lo ciente do agendamento efetuado.</p>


<h3>Linguagens</h3> 
<ul>
<li>PHP</li>
<li>MYSQL</li>
</ul>

<h3>API</h3> 
<ul>
<li>[COMTELE](https://comtele.com.br/)</li>
</ul>

<h3>Instruções</h3>
  <p>As instruções a seguir fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste,
  é necessário a instalação de um servidor Xampp ou semelhantes.</p>

<h2>Clonar</h2>
<p>Para efetuar a clonagem do projeto, utilizando os <i>links</i> disponíveis no repositório GitLab
é nescessário possuir o <i>Software</i> Git instalado na sua máquina. Para mais informações de instalação
acesse o site do [Git](https://git-scm.com/) .Caso você ja possua o Git em sua máquina você pode
baixar este projeto através dos <i>Links</i> disponíveis abaixo.</p>

<h2>Clone com SSH</h2>
 <ul>
 <li>git clone git@gitlab.com:Rafael_Rossales/sistema_distribuido_php.git</li>
 </ul>
 
 <h2>Clone com HTTPS</h2>
 <ul>
 <li>git clone https://gitlab.com/Rafael_Rossales/sistema_distribuido_php.git</li>
 </ul>
 
 <h2>Liscença</h2>
 <p>Este projeto conta com a seguinte licença de uso: Instituto de Tecnologia de <i>Massachusetts</i> [MIT](https://pt.wikipedia.org/wiki/Licen%C3%A7a_MIT).</p>
